---
title: Energy Geostructures
image: /img/e-dean-thermalpower_1116.jpg
overlay: true
description: >
  We utilise numerical modelling techniques, such as finite elements methods, to
  better understand the thermo-mechanical effects relating to the operation of
  these systems, and therefore how they can be most effectively and accurately
  designed, aiming to contribute towards their worldwide utilisation.
---

