---
title: Residential Installation
image: /img/1517971871_projectresidentialinstallation.jpg
overlay: true
description: >-
  A house in Footscray was retrofitted with 2 vertical ground heat exchanger to
  supply fan forced ducted heating and cooling. This 8 kW system was installed
  in 2015 and through a partnership with the University of Melbourne , the
  system has been monitored for the last 2.5 years as part of a bigger
  geothermal systems study in Victoria. This system has been upgraded further
  with a solar thermal collector connected to the boreholes to improve the
  system performance.
---

