---
order: 4
tab: Media
heading: ' '
items:
  - image: 'https://upload.wikimedia.org/wikipedia/en/5/5c/SBS_logo.svg'
    link: >-
      https://www.sbs.com.au/yourlanguage/german/en/audiotrack/switch-what-fourth-element-energy
    source: SBS
  - image: /img/1516794861_logo_IMPACT7.png
    link: 'http://www.impact7.com.au/the-impact7/#linden-jensen-page-project'
    source: Impact7
  - image: /img/1520075133_logo_3RRR.svg
    link: 'https://rrrfm.libsyn.com/einstein-a-go-go-3-september-2017'
    source: Tripe R
  - image: /img/tram-logo.png
    link: 'https://melbconnect.com.au/translating-research-at-tram/'
    source: Translating Research at Melbourne
  - image: /img/scope_logo.jpg
    link: 'https://www.youtube.com/watch?v=9i8sLVuKtkk&feature=youtu.be'
    source: Scope TV
  - image: /img/pursuit.png
    link: 'https://pursuit.unimelb.edu.au/articles/going-underground-for-green-energy'
    source: Pursuit
---

