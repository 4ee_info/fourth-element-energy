---
order: 2
tab: Team
heading: Our Team
image: /img/team-pic.jpg
---
* 30 combined years of experience in shallow geothermal technology
* Delivered $5.4 million in research engagements
* Current or former engineers and researchers from The University of Melbourne (Australia) and Cambridge University (UK)
* Developed world leading, advanced, proprietary design and modelling software allowing any underground structure to be turned into a source of heat
