---
heading: Energy Geostructures
title: Energy Geostructures
image: /img/1517971263_energygeostructures(notunderproject).jpg
---
Underground geo-structures such as piles, tunnel lining and diaphragm walls can be converted into energy geo-structures to facilitate the utilisation of the ground as a renewable, clean and economic source of thermal energy for heating and cooling the buildings. This innovative technology combines the structural role of the geo-structures with the energy supply through integrating ground heat exchangers (high density polyethylene pipes) into these structures. 4EE can provide a complete solution from design to construction and monitoring to access the free energy in the ground via the geo-structures.
