# Fourth Element Energy

This repository is for the Fourth Element Energy web application built by Momentium.

## Hosting + Content Management System

[![Netlify](https://www.netlify.com/img/global/badges/netlify-color-bg.svg)](https://www.netlify.com/)

## Built-in Features

- [Identity](https://www.netlify.com/docs/identity/)
- [Form Handling](https://www.netlify.com/docs/form-handling/)
- [Snippet Injections](https://www.netlify.com/docs/inject-analytics-snippets/)
  - Google analytics
  - GetSiteControl

---

## User Guide

### How to Edit and Publish Content

> Log in via _yourdomain_**/admin** (e.g. https://4ee.netlify.com/admin/) _with trailing slash_

1. Preview and edit your changes
2. Save and publish
3. The website will be automatically rebuilt and deployed

**Note:** You can see the status in your app dashboard
(e.g. https://app.netlify.com/sites/4ee/deploys?filter=master)

### How to Add an Administrator User

- Invite user from your dashboard

### Other

- Netlify supports many other features, including **Email templates** and **Custom domains**. Please check out their website for more information.

---

## Developers

- The solution architectue is based on [JAMstack](https://jamstack.org/)
- Website is built with https://github.com/netlify-templates/one-click-hugo-cms
- Preview templates for all components are developed for Netlify CMS

---

For any other question, please contact Momentium http://momentiumconsulting.com
